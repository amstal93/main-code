# Computer Science Bachelor's Thesis

## Title (english): Use of tools from Infrastructure as Code and a Linux Container Orchestrator to automate and facilitate the deployment of software in a production environment in an IaaS architecture

The aim of this paper is to present a way of creating a secure, efficient, fault-tolerant and initially low-cost production cluster, using the container orchestrator Docker Swarm and some Infrastructure as Code tools, such as Ansible and Terraform.

**Title (portuguese)**: Uso de ferramentas de Infraestrutura como Código e um Orquestrador de Linux contêineres para automatizar e facilitar a implantação de softwares em ambiente de produção em uma arquitetura IaaS

Author: [Mateus Villar](https://mateusvillar.com)

Graduation: [Computer Science](https://unifei.edu.br/prg/cursos-presenciais/itajuba-campus-sede/ciencia-da-computacao/)

Universidade Federal de Itajubá - [UNIFEI](https://unifei.edu.br/)

This is the **main repository** for the **Bachelor's Thesis**.

## All Bachelor's Thesis Repositories

- [pirasbro/bachelors-thesis/main-code](https://gitlab.com/pirasbro/bachelors-thesis/main-code)
- [pirasbro/bachelors-thesis/reverse-proxy](https://gitlab.com/pirasbro/bachelors-thesis/reverse-proxy)
- [pirasbro/bachelors-thesis/webserver](https://gitlab.com/pirasbro/bachelors-thesis/webserver)