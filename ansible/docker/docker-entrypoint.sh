#!/usr/bin/env bash

set -eu;

printf "[INFO docker-entrypoint]: Starting docker entrypoint script..."

printf "\n[INFO docker-entrypoint]: Generating SSH keys for ansible user...\n\n"

ssh-keygen \
    -t rsa \
    -b 4096 \
    -C "${HOSTNAME}_ansible" \
    -N "" \
    -f $HOME/.ssh/ansible_id_rsa

ssh-keygen \
    -t ed25519 \
    -C "${HOSTNAME}_ansible" \
    -N "" \
    -f $HOME/.ssh/ansible_id_ed25519

printf "\n[INFO docker-entrypoint]: Your public generated keys:"

printf "\n\nRSA public key:\n"
ssh-keygen \
    -f $HOME/.ssh/ansible_id_rsa \
    -y
printf "\nED25519 public key:\n"
ssh-keygen \
    -f $HOME/.ssh/ansible_id_ed25519 \
    -y

printf "\nContainer hostname: ${HOSTNAME}"
printf "\nAnsible user identifier: ${HOSTNAME}_ansible"

printf "\n\n[INFO docker-entrypoint]: Script terminating..."

printf "\n[INFO docker-entrypoint]: Container ready for connection."
printf "\n[INFO]: You can use 'docker exec -it ${HOSTNAME} bash' to access this container.\n"

exec "$@"