# This image is based on ansible-community/toolset:
# https://github.com/ansible-community/toolset/blob/fc002246f73b64948f263a559d68f0af75386e9f/Dockerfile
ARG PYTHON_VERSION=3.9.5

# Build requirements.txt stage
FROM python:${PYTHON_VERSION}-slim-buster as builder

ENV PATH "/opt/ansible/bin:$PATH"
ENV PIP_INSTALL_ARGS "--pre"

# Copy requirements.in to container
COPY docker/requirements.in /tmp/requirements.in
RUN set -eux; \
# Install pip and pip-tools
    python3 -m pip install --no-cache-dir --upgrade 'pip>=21.0.1' \
    && python3 -m pip install --no-cache-dir 'pip-tools>=6.0.1' \
# Build requirements.txt file
    && pip-compile \
        --allow-unsafe \
        --output-file=/tmp/requirements.txt \
            /tmp/requirements.in \
# Install requirements
    && python -m venv /opt/ansible \
    && python3 -m pip install --no-cache-dir --upgrade \
        ${PIP_INSTALL_ARGS} --requirement /tmp/requirements.txt


# Final stage
FROM python:${PYTHON_VERSION}-slim-buster as final

ARG EXTRA_PACKAGES
ENV EXTRA_PACKAGES="\
curl \
nano \
vim \
iputils-ping \
"
ENV SHELL /bin/bash
ENV PYTHONDONTWRITEBYTECODE 1
ENV ANSIBLE_FORCE_COLOR 1
ENV PATH "/opt/ansible/bin:$PATH"
ENV SSH_PREFERRED_ENCRYPTION rsa

# Copy packages from builder stage
COPY --from=builder /opt/ansible /opt/ansible

RUN set -eux ; \
# Enable prompt color and some aliases in the skeleton .bashrc
#   before creating ansible user
    sed -e 's/^#force_color_prompt=yes/force_color_prompt=yes/' \
        -e 's/^#export GCC_COLORS=/export GCC_COLORS=/' \
        -e 's/^#alias ll=/alias ll=/' \
        -e 's/^#alias la=/alias la=/' \
        -e 's/^#alias l=/alias l=/' \
            -i /etc/skel/.bashrc ; \
# Create ansible user/group
    addgroup \
        --gid 2727 \
            ansible \
    && adduser \
        --disabled-login \
        --ingroup ansible \
        --gecos "ansible user" \
        --shell /bin/bash \
        --uid 2727 \
            ansible ; \
# Install openssh server, sshpass and tini
    apt-get update \
    && apt-get install -y --no-install-recommends \
        openssh-server \
        sshpass \
        tini ; \
# Initial ansible user setup
    mkdir -p /home/ansible/.ssh \
        /home/ansible/src \
        /home/ansible/test/apps \
    && chown ansible:ansible -R \
        /home/ansible ; \
        # /home/ansible/.ssh ; \
# Install $EXTRA_PACKAGES
    apt-get update \
    && apt-get install -y --no-install-recommends \
        ${EXTRA_PACKAGES} ; \
# Configure argcomplete (ansible autocomplete)
    mkdir -p /home/ansible/.bash_completion.d \
    && chmod 700 /home/ansible/.bash_completion.d/ \
    && chown ansible:ansible -R /home/ansible/.bash_completion.d/ \
    && activate-global-python-argcomplete --dest /home/ansible/.bash_completion.d/ ; \
# Cleanup
    apt-get clean \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*

# Volume for dummy SSH keys
VOLUME [ "/home/ansible/.ssh" ]
# Step down from root
USER ansible
# Change default workspace
WORKDIR /home/ansible/src/
# Copy requirements file to container
COPY requirements.yml .
# Install required collections and roles
RUN ansible-galaxy install --role-file requirements.yml
# Copy src files to container
COPY . .

COPY --chmod=0655 docker/docker-entrypoint.sh /usr/local/bin/docker-ansible-entrypoint
ENTRYPOINT [ "/usr/bin/tini", "-v", "--", "docker-ansible-entrypoint" ]
CMD [ "/bin/bash" ]


# Metadata
# OCI image specs
LABEL org.opencontainers.image.authors="Mateus Villar" \
        org.opencontainers.image.title="Ansible" \
        org.opencontainers.image.description="This image contains the Ansible tool and some helpful packages." \
        org.opencontainers.image.licenses="MIT"