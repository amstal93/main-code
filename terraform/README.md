## Terraform scripts to configure and manage servers

This directory contains two projects with Terraform scripts using AWS provider for automating our project infrastructure.

It also contains an terraform inside a docker container.

## Projects

- backend-state: simple project for setup our remote backed for Terraform using AWS S3 Bucket;
- main: project responsible for Docker Swarm cluster setup on AWS platform;

## AWS Authentication

Follow the steps below:

- Create an AWS account;
- Create an IAM user with administrator privileges;
- Download your key;

Export the following variables containing your IAM user keys on your terminal:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

You can also use other authentication methods found here: [AWS Provider - Authentication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication)

## Backend state project

For team management and better security, we are using the AWS S3 bucket remote backend state with DynamoDB with state locking for Terraform state files.

You can read more about S3 remote backend here: [S3 Backend with locking via DynamoDB](https://www.terraform.io/docs/language/settings/backends/s3.html) 
And more about backend states here: [Backends](https://www.terraform.io/docs/language/settings/backends/index.html)

## Main project

This project uses a community module for deploying a Docker Swarm cluster.

## References:

- [Find a Linux AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html)
- [Data Source: aws_ami](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami)
- [Data Source: aws_vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc)
- [Resource: aws_s3_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
- [Resource: aws_dynamodb_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table)
- [Resource: aws_security_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)
- [Resource: aws_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)
- [AWS Docker Swarm Terraform Module](https://github.com/trajano/terraform-docker-swarm-aws)
- [Terraform + AWS + Docker Swarm setup](https://github.com/alexsedova/terraform-aws-docker)
- [Amazon EC2 AMI Locator](https://cloud-images.ubuntu.com/locator/ec2/)