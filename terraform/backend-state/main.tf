resource "aws_s3_bucket" "prod_pirasbrotfg_backend_state" {
  bucket = "prod-pirasbrotfg-backend-state"

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "prod_pirasbrotfg_backend_lock" {
  name         = "prod_pirasbrotfg_backend_locks"
  billing_mode = "PAY_PER_REQUEST"

  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}