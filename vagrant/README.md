## Vagrant files for quick testing

This directory contain a VagrantFile for quick testing purposes, using VirtualBox as the primary VM provider.

## References:

### Vagrant

- [Loop Over VM Definitions](https://www.vagrantup.com/docs/vagrantfile/tips#loop-over-vm-definitions)
- [Vagrant create multiple VMs loop](https://www.devopsroles.com/vagrant-create-multiple-vms/)